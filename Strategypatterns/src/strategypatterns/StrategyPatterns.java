/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypatterns;
/**
 *
 * @author Fernando
 */
  
public class StrategyPatterns {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
            Calculadora cal;
            cal = new Calculadora();
            System.out.println(  cal.operate( "-", 5, 6 ) );
            System.out.println( cal.operate("@", 4, 5 ) );
            System.out.println( cal.operate("+", 4, 5 ) );
            System.out.println( cal.operate("*", 4, 5 ) );
            System.out.println( cal.operate("/", 6, 5 ) );
        // TODO code application logic here
    }
    
}
