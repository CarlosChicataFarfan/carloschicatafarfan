/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypatterns;
import java.util.*;

abstract class Operation{
    public abstract int operate( int a , int b );
}

class Sum extends Operation{
    @Override
    public int operate( int a , int b ){
        return a+b;
    }
}

class Subtraction extends Operation{
    @Override
    public int operate( int a , int b ){
        return a - b;
    }
}

class Multiplicate extends Operation{
    @Override
    public int operate( int a , int b ){
        return a*b;
    }
}

class Divide extends Operation{
    @Override
    public int operate( int a , int b ){
        return a / b;
    }
}

class Aroba extends Operation{
    @Override
    public int operate( int a , int b ){
        return ( b*( a - 2 ) ) / ( a - b );
    }
    
}

public class Calculadora{
    Map< String, Operation > operaciones; 
   
   public Calculadora(){
        operaciones = new HashMap<String, Operation >();
        operaciones.put("+", new Sum() );
        operaciones.put("-", new Subtraction() );
        operaciones.put( "@", new Aroba() );
        operaciones.put( "*", new Multiplicate() );
        registrar( "/", new Subtraction() );
        ////operaciones.put( "/", new Divide() );
    }
    
    public int operate( String operador, int a , int b ){
         Operation operar = operaciones.get( operador );
         return operar.operate(a, b);
    }
    
    private void registrar( String nombre, Operation operar ){
        operaciones.put(nombre, operar );
        
    }
   
} 
/**
 *
 * @author Fernando
 */

